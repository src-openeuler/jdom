Name:                jdom
Version:             1.1.3
Release:             19
Epoch:               0
Summary:             Java alternative for DOM and SAX
License:             Saxpath
URL:                 http://www.jdom.org/
Source0:             http://jdom.org/dist/binary/archive/jdom-%{version}.tar.gz
Source1:             https://repo1.maven.org/maven2/org/jdom/jdom/%{version}/jdom-%{version}.pom
Patch0000:           jdom-crosslink.patch
Patch0001:           jdom-1.1-OSGiManifest.patch
Patch0002:           CVE-2021-33813.patch

BuildRequires:       ant javapackages-local mvn(jaxen:jaxen) mvn(xerces:xercesImpl)
BuildArch:           noarch

%description
JDOM is a Java representation of an XML document and it provides a way to represent
that document for easy and efficient reading, manipulation, and writing.
It's an alternative to DOM and SAX, although it integrates well with both DOM and SAX.

%package help
Summary:             Help documents for jdom
Provides:            jdom-javadoc = %{version}-%{release}
Obsoletes:           jdom-javadoc < %{version}-%{release}

%description help
The package contains man pages and other related documents for jdom.

%package demo
Summary:             Demos for jdom
Requires:            jdom = %{epoch}:%{version}-%{release}

%description demo
The package contains demonstrations and samples for jdom.

%prep
%autosetup -n jdom -p1
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;

%build
export CLASSPATH=$(build-classpath xerces-j2 jaxen)
ant -Dj2se.apidoc=%{_javadocdir}/java package javadoc-link

%install
%mvn_file : jdom
%mvn_alias : jdom:jdom
%mvn_artifact %{SOURCE1} build/jdom-*-snap.jar
%mvn_install -J build/apidocs
install -d %{buildroot}%{_datadir}/jdom
cp -pr samples %{buildroot}%{_datadir}/jdom

%files -f .mfiles
%doc CHANGES.txt COMMITTERS.txt README.txt TODO.txt LICENSE.txt

%files help -f .mfiles-javadoc
%doc LICENSE.txt

%files demo
%{_datadir}/jdom
%doc LICENSE.txt

%changelog
* Tue Jan 14 2025 pengjian <pengjian23@mails.ucas.ac.cn> - 0:1.1.3-19
- fix CVE-2021-33813

* Mon Jun 8 2020 leiju <leiju4@huawei.com> - 1.1.3-18
- Package init
